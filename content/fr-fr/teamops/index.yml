---
  title: "TeamOps : optimiser l'efficacité de l'équipe | GitLab"
  og_itle: "TeamOps : optimiser l'efficacité de l'équipe | GitLab"
  description: TeamOps est une discipline de gestion d'équipe axée sur les résultats qui réduit les obstacles à la prise de décision pour garantir des exécutions stratégiques rapides et efficaces. Apprendre encore plus!
  twitter_description: TeamOps est une discipline de gestion d'équipe axée sur les résultats qui réduit les obstacles à la prise de décision pour garantir des exécutions stratégiques rapides et efficaces. Apprendre encore plus!
  og_description: TeamOps est une discipline de gestion d'équipe axée sur les résultats qui réduit les obstacles à la prise de décision pour garantir des exécutions stratégiques rapides et efficaces. Apprendre encore plus!
  og_image: /nuxt-images/open-graph/teamops-opengraph.png
  twitter_image: /nuxt-images/open-graph/teamops-opengraph.png
  hero:
    logo:
      show: true
    title: |
      De meilleures équipes.
      Progression plus rapide.
      Monde meilleur.
    subtitle: Faire du travail d'équipe une discipline objective
    aos_animation: fade-down
    aos_duration: 1600
    aos_offset: 200
    image:
      url: /nuxt-images/team-ops/hero-illustration.png
      alt: image de héros des opérations d'équipe
      aos_animation: zoom-out-left
      aos_duration: 1600
      aos_offset: 200
    button:
      href: https://levelup.gitlab.com/learn/course/teamops
      text: Inscrivez votre équipe
      data_ga_name: enroll your team
      data_ga_location: body
  spotlight:
    title: |
      TeamOps stimule la vitesse de décision
    subtitle: TeamOps est un modèle d'exploitation organisationnel qui aide les équipes à maximiser la productivité, la flexibilité et l'autonomie en gérant plus efficacement les décisions, les informations et les tâches.
    description:
      "Créer un environnement propice à de meilleures décisions et à une meilleure exécution de celles-ci permet d'améliorer les équipes et, en fin de compte, de progresser.\n\n\n
       TeamOps est la façon dont GitLab est passé d'une startup à une entreprise publique mondiale en une décennie. Maintenant, nous l'ouvrons à toutes les organisations."
    list:
      title: Points douloureux courants
      items:
        - Retards de prise de décision
        - Fatigue de rencontre
        - Mauvaise communication interne
        - Transferts lents et retards de flux de travail
      icon: warning
    button:
      href: https://levelup.gitlab.com/learn/course/teamops
      text: Améliorez votre équipe
      data_ga_name: make your team better
      data_ga_location: body
    aos_animation: fade-up
    aos_duration: 1600
    aos_offset: 200
  features:
    title: |
      Conçu pour toutes les équipes.
      À distance, hybride ou bureau.
    image:
      url: /nuxt-images/team-ops/reasons-to-believe.png
      alt: Raisons de croire en l'image de Team Ops
    accordion:
      is_accordion: false
      items:
        - icon:
            name: group
            alt: User Group Icon
            variant: marketing
          header: TeamOps est une nouvelle discipline de gestion axée sur les objectifs et les résultats
          text: TeamOps aide les organisations à progresser davantage en traitant les relations des membres de votre équipe comme un problème pouvant être opérationnalisé.
        - icon:
            name: clipboard-check-alt
            alt: Clipboard Checkmar Icon
            variant: marketing
          header: Un système testé sur le terrain
          text: Nous construisons et utilisons TeamOps chez GitLab depuis 5 ans. Par conséquent, notre organisation est plus productive et les membres de notre équipe témoignent d'une plus grande satisfaction au travail. Il a été créé ici, mais nous pensons qu'il peut aider presque toutes les organisations.
        - icon:
            name: principles
            alt: Continuous Integration Icon
            variant: marketing
          header: Des principes directeurs
          text: TeamOps est fondé sur quatre principes directeurs qui peuvent aider les organisations à naviguer de manière rationnelle dans la nature dynamique et changeante du travail.
        - icon:
            name: cog-user-alt
            alt: Cog User Icon
            variant: marketing
          header: Principes d'action
          text: Chaque principe est soutenu par un ensemble de principes d'action - des méthodes de travail basées sur le comportement qui peuvent être mises en œuvre immédiatement.
        - icon:
            name: case-study-alt
            alt: Case Study Icon
            variant: marketing
          header: Exemples concrets
          text: Nous donnons vie aux principes d'action avec une bibliothèque croissante d'exemples basés sur des résultats réels de ce principe tel qu'il est pratiqué chez Gitlab.
        - icon:
            name: verification
            alt: Ribbon Check Icon
            variant: marketing
          header: Cours TeamOps
          text: Nous créons un alignement grâce au cours TeamOps, permettant aux équipes et aux entreprises de découvrir le cadre dans un environnement partagé.
    aos_animation: fade-up
    aos_duration: 1600
    aos_offset: 200
    accordion_aos_animation: fade-left
    accordion_aos_duration: 1600
    aaccordion_os_offset: 200
  video_spotlight:
    title: |
      Immergez-vous dans TeamOps
    subtitle: Le monde a beaucoup d'opinions sur l'avenir du travail.
    description:
      "Le cours TeamOps aide les organisations à progresser davantage en traitant les relations des membres de votre équipe comme un problème pouvant être opérationnalisé.\n\n\n
       En quelques heures seulement, vous pouvez approfondir chacun des principes directeurs et principes d'action du modèle, et commencer à évaluer la compatibilité avec les pratiques opérationnelles actuelles de votre équipe."
    video:
      url: 754916142?h=56dd8a7d5d
      alt: image de héros des opérations d'équipe
    button:
      href: https://levelup.gitlab.com/learn/course/teamops
      text: Inscrivez-vous maintenant
      data_ga_name: enroll your team
      data_ga_location: body
    aos_animation: fade-up
    aos_duration: 1600
    aos_offset: 200
  card_section:
    title: |
      Principes directeurs de TeamOps
    subtitle: Les organisations ont besoin de personnes et d'équipes, de leur créativité, de leurs perspectives et de leur humanité.
    cards:
      - title: Réalité partagée
        description: |
          Alors que d'autres philosophies de gestion donnent la priorité à la vitesse de transfert des connaissances, TeamOps optimise la vitesse de récupération des connaissances.
        icon:
          name: d-and-i
          slp_color: surface-700
        link:
          text: Apprendre encore plus
          url: /handbook/teamops/shared-reality/
        color: '#FCA326'
      - title: Tout le monde contribue
        description: |
          Les organisations doivent créer un système où chacun peut consommer des informations et contribuer, quel que soit son niveau, sa fonction ou son emplacement.
        icon:
          name: user-collaboration
          slp_color: surface-700
        link:
          text: Apprendre encore plus
          url: /handbook/teamops/everyone-contributes/
        color: '#966DD9'
      - title: Vitesse de décision
        description: |
          Le succès est corrélé à la vitesse de décision : la quantité de décisions prises dans un laps de temps donné et les résultats qui découlent d'une progression plus rapide.
        icon:
          name: speed-alt-2
          slp_color: surface-700
        link:
          text: Apprendre encore plus
          url: /handbook/teamops/decision-velocity/
        color: '#FD8249'
      - title: Clarté des mesures
        description: |
          Il s'agit de mesurer les bonnes choses. Les principes décisionnels de TeamOps ne sont utiles que si vous exécutez et mesurez les résultats.
        icon:
          name: target
          slp_color: surface-700
        link:
          text: Apprendre encore plus
          url: /handbook/teamops/measurement-clarity/
        color: '#256AD1'
    aos_animation: fade-up
    aos_duration: 1600
    aos_offset: 200
  join_us:
    title: |
      Rejoignez le mouvement
    description:
      "TeamOps est un modèle d'exploitation organisationnel qui aide les équipes à maximiser la productivité, la flexibilité et l'autonomie en gérant plus efficacement les décisions, les informations et les tâches. \n\n\n
       Rejoignez une liste croissante d'organisations qui pratiquent TeamOps."
    list:
      title: Points douloureux courants
      items:
        - Les flux de travail ad hoc empêchent l'alignement
        - La gestion du bricolage engendre des dysfonctionnements
        - L'infrastructure de communication est une réflexion après coup
        - L'obsession du consensus contrecarre l'innovation
      icon: warning
    button:
      href: https://levelup.gitlab.com/learn/course/teamops
      text: Inscrivez-vous maintenant
      data_ga_name: make your team better
      data_ga_location: body
    quotes:
      - text: Les tests de pré-déploiement ont renforcé la confiance dans le fait que le produit est prêt à être publié ; la fréquence de livraison a également augmenté.
        author: Nom de Jean
        note: Directeur du titre du poste, nom de l'entreprise
      - text: Les tests de pré-déploiement ont renforcé la confiance dans le fait que le produit est prêt à être publié ; la fréquence de livraison a également augmenté.
        author: Nom de Jean
        note: Directeur du titre du poste, nom de l'entreprise
    clients:
      - logo: /nuxt-images/home/logo_cncf_mono.svg
        alt: Cloud Native Logo
      - logo: /nuxt-images/home/logo_tmobile_mono.svg
        alt: Cloud Native Logo
      - logo: /nuxt-images/home/logo_goldman_sachs_mono.svg
        alt: Goldman Sachs logo
      - logo: /nuxt-images/home/logo_siemens_mono.svg
        alt: Siemens logo
      - logo: /nuxt-images/home/logo_tmobile_mono.svg
        alt: Cloud Native Logo
      - logo: /nuxt-images/team-ops/logo_knowbe4_mono.svg
        alt: KnowBe4 logo
      - logo: /nuxt-images/home/logo_cncf_mono.svg
        alt: Cloud Native Logo
      - logo: /nuxt-images/home/logo_tmobile_mono.svg
        alt: Cloud Native Logo
      - logo: /nuxt-images/home/logo_goldman_sachs_mono.svg
        alt: Goldman Sachs logo
    aos_animation: fade-up
    aos_duration: 1600
    aos_offset: 200
