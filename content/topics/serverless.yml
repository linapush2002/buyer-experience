---
  title: What is serverless?
  description: Serverless software architecture uses cloud managed services and event driven code, allowing developers to build scalable and cost-efficient applications
  topic_name: Serverless
  icon: automated-code
  topics_header:
      data:
        title: What is serverless?
        two_col: true
        block:
          - metadata:
              id_tag: what-is-serverless
            text: |
              Serverless is a software architecture design pattern that takes advantage of event-driven code execution powered by cloud managed services to build massively scalable and cost-efficient applications composed of small discrete functions without developers needing to design for or think about the underlying infrastructure where their code runs.
            link_text: Learn more about GitLab
            link_href: /why-gitlab/
            data_ga_name: Learn more about GitLab
            data_ga_location: header
  crumbs:
    - title: Topics
      href: /topics/
      data_ga_name: topics
      data_ga_location: breadcrumb
    - title: Serverless
  side_menu:
    anchors:
      text: "On this page"
      data:
        - text: What is serverless business logic?
          href: "#what-is-serverless-business-logic"
          data_ga_name: what is serverless business logic
          data_ga_location: side-navigation
          variant: primary
        - text: Attributes of serverless
          href: "#attributes-of-serverless"
          data_ga_name: attributes of serverless
          data_ga_location: side-navigation
        - text: Comparison of cloud managed services
          href: "#comparison-of-cloud-managed-services"
          data_ga_name: comparison of cloud managed services
          data_ga_location: side-navigation
        - text: Business Values and Benefits of GitLab Serverless
          href: "#business-values-and-benefits-of-git-lab-serverless"
          data_ga_name: business values and benefits of git lab serverless
          data_ga_location: side-navigation
    hyperlinks:
      text: ''
      data: []
    content:
      - name: topics-copy-block
        data:
          column_size: 10
          header: What is serverless business logic?
          blocks:
            - text: |
                So, what is serverless business logic? Every application uses servers at some point. The term Serverless emphasizes an architecture and service model where the developers need not concern themselves with infrastructure and instead can focus on the business logic of their appliction. Serverless is the next evolution of architectural design from monolith, to [microservices](/topics/microservices/){data-ga-name="Microservices" data-ga-location="body"}, to functions as Adrian Cockcroft explains in this video. 
              video:
                video_url: https://www.youtube-nocookie.com/embed/aBcG57Gw9k0
           

      - name: topics-copy-block
        data:
          column_size: 10
          header: Serverless, FaaS (Functions as a service), and managed services
          blocks:
            - text: |
                Often serverless and FaaS are treated as interchangeable terms, but this isn't really accurate. Serverless is an overarching architectural pattern that makes use of a FaaS along with other cloud managed services. FaaS is a specific type of service such as AWS Lambda, Google Cloud Functions, and Azure Functions, that enables developers to deploy functions.
              metadata:
                id_tag: serverless-faas-managed-services
              
      - name: topics-copy-block
        data:
          column_size: 10
          header: Attributes of serverless
          blocks:
            - text: |
                1. Small, discrete units of code. Often services written using serverless architecture are comprised of a single function.

                2. Event-based execution. The infrastructure needed to run a function doesn't exist until a function is triggered. Once an event is received an ephemeral compute environment is created to execute that request. The environment may be destroyed immediately, or more commonly stays active for a short period of time, commonly 5 minutes.

                3. Scale to zero. Once a function stops receiving requests the infrastructure is taken down and completely stops running. This saves on cost since the infrastructure only runs when there is usage. If there's no usage, the environment scales down to zero.

                4. Scale to infinity. The FaaS takes care of monitoring load and creating additional instances when needed, in theory, up to infinity. This virtually eliminates the need for developers to think about scale as they design applications. A single deployed function can handle one or one billion requests without any change to the code.

                5. Use of managed services. Often serverless architectures make use of cloud provided services for elements of their application that provide non-differentiated heavy lifting such as file storage, databases, queueing, etc. For example, Google's Firebase is popular in the serverless community as a database and state management service that connects to other Google services like Cloud Functions.
              metadata:
                id_tag: attributes-of-serverless
      - name: topics-copy-block
        data:
          column_size: 10
          header: Comparison of cloud managed services
          blocks:
            - text: Here is a chart of with examples of managed services from AWS, Google Cloud, and Azure along with their open source counterparts.
      - name: 'slp-table'
        data:
          column_size: 10
          full_width: true
          headers:
            - Service
            - Open Source
            - AWS
            - Google Cloud
            - Azure
          rows:
            - columns:
                - FaaS
                - Knative
                - Lambda
                - Cloud Functions
                - Azure Functions
            - columns:
                - Storage
                - Minio
                - S3
                - Cloud storage
                - Azure storage
            - columns:
                - SQL DB
                - MySQL
                - RDS
                - Cloud SQL
                - Azure SQL Database
            - columns:
                - NoSQL DB
                - MongoDB, Cassandra, CouchDB
                - DynamoDB
                - Cloud Datastore
                - Cosmos DB
            - columns:
                - Message queue
                - Kafka, Redis, RabbitMQ
                - SQS, Kinesis
                - Google Pub/Sub
                - Azure Queue Storage
            - columns:
              - Service mesh
              - Istio
              - App Mesh
              - Istio on GKE
              - Azure Service Fabric Mesh
      - name: topics-copy-block
        data:
          column-size: 10
          header: Business Values and Benefits of GitLab Serverless
          blocks:
            - text: GitLab Serverless allows businesses to deploy their own FaaS on Kubernetes.
      - name: benefits-icons
        data:
          use_icon_component: true
          column_size: 10
          benefits:
            - title: Faster
              icon:
                name: gitlab-rocket
                variant: marketing
                alt: GitLab Rocket Icon
              text: Faster pace of innovation. Developer productivity increases when they can focus solely on business logic.

            - title: Stability
              icon:
                name: stable-computer
                variant: marketing
                alt: Stable Computer Icon
              text: Greater stability/resiliency (less loss of revenue due to downtime).

            - title: Scale
              icon:
                name: scale
                variant: marketing
                alt: Scale Icon
              text: Greater scale, the software is able to keep up with business demand.

            - title: Cost
              icon:
                name: piggy-bank
                variant: marketing
                alt: Piggy Bank Icon
              text: Lower costs. Since compute is only billed when a service is active, servless provides tremendous cost savings vs always-on infrastructure.

            - title: No vendor lock-in
              icon:
                name: cloud
                variant: marketing
                alt: Cloud Icon
              text: No vendor lock-in. Organizations can choose who they want to run their compute. In any cloud that supports Kubernetes, or even on-premises servers.

            - title: Workflow
              icon:
                name: continuous-integration
                variant: marketing
                alt: Continuous Integration Icon
              text: Your FaaS is part of the same workflow as the rest of your software lifecyle with a single appliction from planning and testing, to deployment and monitoring.

            - title: Deployment
              icon:
                name: gitlab-pipeline
                variant: marketing
                alt: GitLab Pipeline Icon
              text: Deploying functions is greatly streamlined and simplified vs using Knative directly.
  components:
    - name: solutions-resource-cards
      data:
        title: Additional resources
        column_size: 4
        grouped: false
        cards:
          - icon:
              name: blog
              variant: marketing
              alt: blog Icon
            event_type: Blog
            header: What is Serverless Architecture? What are its Pros and Cons?
            link_text: Learn more
            image: "/nuxt-images/resources/resources_4.jpeg"
            href: https://hackernoon.com/what-is-serverless-architecture-what-are-its-pros-and-cons-cc4b804022e9
            data_ga_name: What is Serverless Architecture? What are its Pros and Cons?
            data_ga_location: resource cards
          - icon:
              name: case-study
              variant: marketing
              alt: case study icon
            event_type: Case study
            header: Knative
            link_text: Learn more
            image: "/nuxt-images/resources/resources_11.jpeg"
            href: https://cloud.google.com/knative/
            data_ga_name: Knative
            data_ga_location: resource cards
          - icon:
              name: blog
              variant: marketing
              alt: Blog Icon
            event_type: Blog
            header: Martin Folwer on serverless architectures
            link_text: Learn more
            image: "/nuxt-images/resources/resources_9.jpeg"
            href: https://martinfowler.com/articles/serverless.html
            data_ga_name: Martin Folwer on serverless architectures
            data_ga_location: resource cards

    - name: solutions-resource-cards
      data:
        title: Suggested Content
        column_size: 4
        cards:
          - icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            header: Announcing GitLab Serverless deploying to Cloud Run for Anthos
            text: Discover how we're making it easier to deploy serverless workloads on-premise with Anthos.
            link_text: "Learn more"
            href: /blog/2019/11/19/gitlab-serverless-with-cloudrun-for-anthos/
            image: /nuxt-images/blogimages/gitlab-serverless-blog.png
            data_ga_name: Announcing GitLab Serverless deploying to Cloud Run for Anthos
            data_ga_location: resource cards
          - icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            header: Is serverless the end of ops?
            text: What is Serverless architecture, what are the pros and cons of using it and where will it go in the future?
            link_text: "Learn more"
            href: /blog/2019/09/12/is-serverless-the-end-of-ops/
            image: /nuxt-images/blogimages/serverless-ops-blog.jpg
            data_ga_name: Is serverless the end of ops?
            data_ga_location: resource cards
          - icon:
              name: web
              variant: marketing
              alt: Web Icon
            event_type: "Web"
            header: How to deploy AWS Lambda applications with ease
            text: Highlights from our serverless webcast with AWS exploring the Serverless Application Model.
            link_text: "Learn more"
            href: /blog/2020/04/29/aws-gitlab-serverless-webcast/
            image: /nuxt-images/default-blog-image.png
            data_ga_name: How to deploy AWS Lambda applications with ease
            data_ga_location: resource cards
